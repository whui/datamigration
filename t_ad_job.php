<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-12
 * Time: 上午10:56
 * To change this template use File | Settings | File Templates.
 */
echo "t_ad_job";
$input_sql = "SELECT
	`adid` AS `ad_id`,
	`jobtype`,
	`at_every`,
	`jobtime`,
	`jobrmb`,
	`create_user`,
	`create_time`,
	a.`is_run`,
	`linkid`,
	IFNULL (j.url,'') AS url
	FROM
	`t_ad_job` AS a
	LEFT JOIN t_job AS j
	ON a.linkid = j.id
	AND j.url LIKE 'event%'"; //

$output_sql ="INSERT INTO `t_ad_job`
	(
	`ad_id`,
	`jobtype`,
	`at_every`,
	`jobtime`,
	`jobrmb`,
	`create_user`,
	`create_time`,
	`is_run`,
	`linkid`
	)
	VALUES
	";
$link_id_sql="SELECT
    `url`,
    `id`
	FROM
	`t_job`";

$old = include(dirname(__FILE__)."/inc/pdo_old.php");
$new = include(dirname(__FILE__)."/inc/pdo_new.php");

$url_ids=$new->query($link_id_sql)->fetchAll(PDO::FETCH_KEY_PAIR);
$input=$old->query($input_sql)->fetchAll(PDO::FETCH_ASSOC);
$result=array();
foreach($input as $k=>$v)
{
    if(array_key_exists($v["url"],$url_ids))
    {
        $v["linkid"]=$url_ids[$v["url"]];
        unset($v["url"]);
        $result[$k]=$v;
    }
    else
    {
        unset($v["url"]);
        $result[$k]=$v;
    }
}
$values=array();
foreach((array)$result as $line){
    $values[]="('".implode("','",$line)."')";
}

if($values){
    $valuesql=implode(",",$values);
    $sql = $output_sql.$valuesql;
    $count=$new->exec($sql);
    if(!$count)
    {
        echo 'return false';
        echo $sql;
    }
    echo $count;
}
echo 'finish';

$old=null;
$new=null;

?>