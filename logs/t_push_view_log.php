<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-15
 * Time: 下午5:09
 * To change this template use File | Settings | File Templates.
 */


include_once("../inc/migrate.class.php");
$input_sql = "SELECT
	`device_id`,
	`app_id`,
	`ad_id`,
	`view_time`,
	`pack_name`,
	`push_type`,
	`sdk_version`

	FROM
	`t_push_view_log`
	WHERE view_time>DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00')"; //
$outpt_sql ="INSERT INTO `t_push_view_log`
	(
	`device_id`,
	`app_id`,
	`ad_id`,
	`view_time`,
	`pack_name`,
	`push_type`,
	`sdk_version`
	)
	VALUES
	";
$mig = new migrate("t_push_view_log",$input_sql,$outpt_sql);
$mig->process();