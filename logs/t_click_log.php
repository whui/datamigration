<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-15
 * Time: 下午3:03
 * To change this template use File | Settings | File Templates.
 */

include_once("../inc/migrate.class.php");
$input_sql = "SELECT
	`deviceid` AS `device_id`,
	`appid` AS `app_id`,
	`adid` AS `ad_id`,
	`channel`,
	`click_time`,
	`packagename`,
	`sdkversion`
	FROM
	`t_click_log`
	WHERE click_time>DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00')"; //
$outpt_sql ="INSERT INTO `t_offer_click_log`
	(
	`device_id`,
	`app_id`,
	`ad_id`,
	`channel`,
	`click_time`,
	`packagename`,
	`sdkversion`
	)
	VALUES
	";
$mig = new migrate("t_click",$input_sql,$outpt_sql);
$mig->process();
?>