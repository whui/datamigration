<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-12
 * Time: 下午2:44
 * To change this template use File | Settings | File Templates.
 */

include_once("inc/migrate_by_day.class.php");
$input_sql = "SELECT
	`app_id`,
	'%s' AS  `view_date`,
	SUM(nums_pv) AS `nums_pv`,
	`push_type`
	FROM
	`t_push_view_log_stat`
	WHERE view_date='%s' AND view_h=24
	GROUP BY app_id,`push_type`;";
$outpt_sql ="INSERT INTO `s_push_view_stat`
	(
	`app_id`,
	`view_date`,
	`nums_pv`,
	`push_type`
	)
	VALUES";
$mig = new migrate_by_day("s_push_view_stat",$input_sql,$outpt_sql,"view_date","-1 year");
$mig->process();
?>