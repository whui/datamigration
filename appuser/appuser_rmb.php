<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-18
 * Time: 下午1:20
 * To change this template use File | Settings | File Templates.
 */
echo "t_appuser/t_appuser_rmb start at ";
echo date("Y-m-d H:i:s");

$dbh_old = include(dirname(__FILE__)."/../inc/pdo_old.php");
$dbh_new = include(dirname(__FILE__)."/../inc/pdo_new.php");
$end=date("Y-m-d");
$start=date('Y-m-d',strtotime("-2 day"));
//第一部分，两天之内的数据全倒，一天约20W行


//第二部分，三天到三个月内的，畅无线和米赚的全倒，其他的rmb>0也倒
$end=date('Y-m-d',strtotime("-3 day"));
$start=date('Y-m-d',strtotime("-90 day"));

for($i=strtotime($start);$i<=strtotime($end);$i+=86400)
{
    $date = date("Y-m-d",$i);
    insert_one_day($date,$dbh_old,$dbh_new);
}

//第三部分，三个月到半年的，畅无线和米赚的全倒，其他的全不要
$end=date('Y-m-d',strtotime("-91 day"));
$start=date('Y-m-d',strtotime("-180 day"));

$dbh_old=null;
$dbh_new=null;
echo 't_appuser finish at';
echo date("Y-m-d H:i:s");

function insert_one_day($date,$dbh_old,$dbh_new)
{
    $today=date("Y-m-d");
    echo $date;
    for($h=0;$h<24;$h++)
    {
        echo "hour:$h";
        $sql = "select * from t_appuser where first_time>='$date $h:00:00' and first_time<'$date $h:59:59'";
        echo "select start:".date("Y-m-d H:i:s");
        $result=$dbh_old->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        echo "select end:".date("Y-m-d H:i:s");
        $sp_apps=array ('024d7dcbd50ed891cb30662b13f9bb79','bd0b0498d7c85e60c37360713dd92242','d7ef1da6964dc672b8077f2caeacec86');
        $appusers=array();
        $appuser_rmbs=array();
        $output_sql = "INSERT INTO `t_appuser`
        (`appid_key`,
        `app_id`,
        `device_id`,
        `first_date`,
        `last_date`,
        `ua`
        )
        VALUES";
        $output_sql_rmb="INSERT INTO `t_appuser_rmb`
        (`app_id`,
        `device_id`,
        `pts`,
        `pts_in`,
        `pts_out`,
        `last_date`
        )
        VALUES";

        foreach((array)$result as $line){
//            if(insert_appuser($dbh_new,$appusers,2000,$output_sql))
//            {
//                unset($appusers);
//            }
//            if(insert_appuser_rmb($dbh_new,$appuser_rmbs,2000,$output_sql_rmb))
//            {
//                unset($appuser_rmbs);
//            }
            if((int)$line["rmb"] > 0)
            {
                $appusers[]="('".$line['appid_key']."'
            ,'".$line['appid']."'
            ,'".$line['deviceid']."'
            ,'".substr($line['first_time'],0,10)."'
            ,'$today'
            ,'".str_replace("'","\'", $line["ua"])."'
            )";
                $appuser_rmbs[]="('".$line['appid']."'
            ,'".$line['deviceid']."'
            ,'".$line['rmb']."'
            ,'".$line['rmb_in']."'
            ,'".$line['rmb_out']."'
            ,'$today'
            )";
                continue;
            }
            if(in_array($line["appid"],$sp_apps))//前面已经截取了所有rmb>0的，所以此处不用再截取了
            {
                $appusers[]="('".$line['appid_key']."'
            ,'".$line['appid']."'
            ,'".$line['deviceid']."'
            ,'".substr($line['first_time'],0,10)."'
            ,'$today'
            ,'".str_replace("'","\'",$line['ua'])."'
            )";
                continue;
            }
        }
        if(insert_appuser($dbh_new,$appusers,0,$output_sql))
        {
            unset($appusers);
        }
        if(insert_appuser_rmb($dbh_new,$appuser_rmbs,0,$output_sql_rmb))
        {
            unset($appuser_rmbs);
        }

    }

}

function insert_appuser($dbh_new,&$arr,$min_num,$output_sql)
{
    if(count($arr)>$min_num)
    {
        $valuesql=implode(",",$arr);
        $sql = $output_sql.$valuesql;
        echo "insert start:".date("Y-m-d H:i:s");
        $count=$dbh_new->exec($sql);
        echo "insert end:".date("Y-m-d H:i:s");
        if(!$count)
        {
            echo "insert false or zero:";
            echo $sql;
        }
        echo $count;
        echo"|";
        return true;
    }
    return false;
}

function insert_appuser_rmb($dbh_new,&$arr_rmb,$min_num,$output_sql_rmb)
{
    if(count($arr_rmb)>$min_num)
    {
        $valuesql=implode(",",$arr_rmb);
        $sql = $output_sql_rmb.$valuesql;
        echo "insert start:".date("Y-m-d H:i:s");
        $count=$dbh_new->exec($sql);
        echo "insert end:".date("Y-m-d H:i:s");
        if(!$count)
        {
            echo "insert false or zero:";
            echo $sql;
        }
        echo $count;
        echo"|";
        return true;
    }
    return false;
}
?>