<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-11
 * Time: 下午6:00
 * To change this template use File | Settings | File Templates.
 */


$dbh_old = include("inc/pdo_old.php");
$dbh_new = include("inc/pdo_new.php");
$sql = "SELECT 	`id`,
	`userid` as `user_id`,
	`app_type`,
	`appname`,
	`status`,
	`rmb_in`,
	`create_time`,
	`app_icon`,
	`app_path`,
	`app_version`,
	`up_info`,
	`app_money`,
	`app_ratio`,
	`app_tags`,
	`package_name`,
	`list_bg`,
	`rmb_switch`,
	`upload_app`,
	`app_desc`,
	`app_pic`,
	`open_give`,
	`update_time`,
	`level`,
	`tags`,
	`push_status`,
	`push_interval`,
	`push_delay`,
	`push_infovisible`,
	`daily_limit`,
	`clearable`
	FROM
	`t_appinfo` ;"; //
$result=$dbh_old->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$values=null;
foreach((array)$result as $line){
    $line["app_desc"]=str_replace("'","\'", $line["app_desc"]); //单引号
    $line["app_version"]=strip_tags($line["app_version"]); //有sql注入的<script>类型的字符串存在
    $values[]="('".implode("','",$line)."')";
}
if($values){
    $valuesql=implode(",",$values);
    $sql = "INSERT INTO `t_appinfo`
	(`id`,
	`user_id`,
	`app_type`,
	`appname`,
	`status`,
	`rmb_in`,
	`create_time`,
	`app_icon`,
	`app_path`,
	`app_version`,
	`up_info`,
	`app_money`,
	`app_ratio`,
	`app_tags`,
	`package_name`,
	`list_bg`,
	`rmb_switch`,
	`upload_app`,
	`app_desc`,
	`app_pic`,
	`open_give`,
	`update_time`,
	`level`,
	`tags`,
	`push_status`,
	`push_interval`,
	`push_delay`,
	`push_infovisible`,
	`daily_limit`,
	`clearable`
	)
	VALUES
	$valuesql
	";

    echo "t_appinfo";
    $count=$dbh_new->exec($sql);
    if($count)
    {
        echo "false";
    }
    echo $count;
}

$dbh_old=null;
$dbh_new=null;
echo 'ok';
?>