<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-25
 * Time: 上午2:42
 * To change this template use File | Settings | File Templates.
 */

$dbh_old = include(dirname(__FILE__)."/inc/pdo_old.php");

$dbh_new = include(dirname(__FILE__)."/inc/pdo_new.php");

$input_sql = "SELECT
	`app_id`,
	'2013-4-25' AS `push_date`,
	SUM(`nums_pv`) AS `nums_pv`,
	`push_type`
	FROM
	`t_push_log_stat`
	WHERE `push_date`='2013-4-25' AND push_h =24
	GROUP BY app_id,push_type";
$output_sql ="INSERT INTO `s_push_stat_app`
	(
	`app_id`,
	`push_date`,
	`nums_pv`,
	`push_type`
	)
	values";

$result=$dbh_old->query($input_sql)->fetchAll(PDO::FETCH_ASSOC);
$values=array();
foreach((array)$result as $line){
    $values[]="('".implode("','",$line)."')";
}
if($values){
    $valuesql=implode(",",$values);
    $sql = $output_sql.$valuesql.' on duplicate key update nums_pv=nums_pv+values(nums_pv)';
    $count=$dbh_new->exec($sql);
    if(!$count)
    {
        echo "insert false or zero:";
        echo $sql;
    }
    echo $count;
}


$input_sql = "SELECT
	`app_id`,
	'2013-04-25' AS `push_date`,
	push_h,
	SUM(`nums_pv`) AS `nums_pv`,
	`push_type`
	FROM
	`t_push_log_stat`
	WHERE `push_date`='2013-04-25' AND push_h <24
	GROUP BY app_id,push_type,push_h";
$output_sql ="INSERT INTO `s_push_stat_app_h`
	(
	`app_id`,
	`push_date`,
	`push_h`,
	`nums_pv`,
	`push_type`
	)
	values";

$result2=$dbh_old->query($input_sql)->fetchAll(PDO::FETCH_ASSOC);
$values2=array();
foreach((array)$result2 as $line){
    $values2[]="('".implode("','",$line)."')";
}
if($values2){
    $valuesql=implode(",",$values2);
    $sql = $output_sql.$valuesql;
    $count=$dbh_new->exec($sql);
    if(!$count)
    {
        echo "insert false or zero:";
        echo $sql;
    }
    echo $count;
}