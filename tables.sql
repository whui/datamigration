/*
SQLyog Community v11.01 (64 bit)
MySQL - 5.5.16 : Database - dianjoynew
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `log_parser` */

DROP TABLE IF EXISTS `log_parser`;

CREATE TABLE `log_parser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stat_time` datetime NOT NULL,
  `param` varchar(100) NOT NULL,
  `hit` int(11) NOT NULL DEFAULT '0',
  `avgtime` int(11) NOT NULL DEFAULT '0',
  `mintime` int(11) NOT NULL DEFAULT '0',
  `maxtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `m_sys` */

DROP TABLE IF EXISTS `m_sys`;

CREATE TABLE `m_sys` (
  `k` varchar(20) NOT NULL COMMENT '键',
  `v` varchar(30) NOT NULL COMMENT '值',
  PRIMARY KEY (`k`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 MAX_ROWS=20 COMMENT='系统运行内存表';

/*Table structure for table `s_appuser_stat` */

DROP TABLE IF EXISTS `s_appuser_stat`;

CREATE TABLE `s_appuser_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `add_date` date NOT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `nums` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appid_date` (`app_id`,`add_date`,`channel`),
  KEY `date_channel` (`add_date`,`channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天统计新用户保留一年';

/*Table structure for table `s_appuser_stat_h` */

DROP TABLE IF EXISTS `s_appuser_stat_h`;

CREATE TABLE `s_appuser_stat_h` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `add_date` date NOT NULL,
  `add_h` int(11) NOT NULL,
  `nums` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `appid_date_h` (`add_date`,`app_id`,`add_h`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按小时统计新用户保留三月';

/*Table structure for table `s_appuser_stat_today` */

DROP TABLE IF EXISTS `s_appuser_stat_today`;

CREATE TABLE `s_appuser_stat_today` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `add_date` date NOT NULL,
  `add_h` int(11) NOT NULL DEFAULT '24' COMMENT '24代表一天的汇总，其他代表小时',
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `nums` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app` (`add_date`,`app_id`,`add_h`,`channel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='今日新用户';

/*Table structure for table `s_cash_history` */

DROP TABLE IF EXISTS `s_cash_history`;

CREATE TABLE `s_cash_history` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `user_id` char(32) NOT NULL,
  `rmb` int(11) DEFAULT '0' COMMENT '真实货币，以分为单位',
  `cash_time` datetime DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `appid_date` (`cash_time`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `s_offer_downok_stat` */

DROP TABLE IF EXISTS `s_offer_downok_stat`;

CREATE TABLE `s_offer_downok_stat` (
  `id` BIGINT(22) NOT NULL AUTO_INCREMENT,
  `app_id` CHAR(32) NOT NULL,
  `down_date` DATE NOT NULL,
  `down_total` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `down_date` (`down_date`,`app_id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='下载完成数统计';

/*Table structure for table `s_offer_adview_stat` */
DROP TABLE IF EXISTS `s_offer_adview_stat`;

CREATE TABLE `s_offer_adview_stat` (
  `id` BIGINT(22) NOT NULL AUTO_INCREMENT,
  `app_id` CHAR(32) NOT NULL,
  `view_date` DATE NOT NULL,
  `view_total` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `view_date` (`view_date`,`app_id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='查看数统计';

/*Table structure for table `s_offer_adlist_stat` */

DROP TABLE IF EXISTS `s_offer_adlist_stat`;

CREATE TABLE `s_offer_adlist_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `click_date` date NOT NULL,
  `click_total` int(11) NOT NULL COMMENT '开墙数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `click_date` (`click_date`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='开墙数统计';

/*Table structure for table `s_offer_click_log_stat` */

DROP TABLE IF EXISTS `s_offer_click_log_stat`;

CREATE TABLE `s_offer_click_log_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `click_date` date NOT NULL,
  `click_total` int(11) NOT NULL COMMENT '点击数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `click_date` (`click_date`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='广告点击统计';

DROP TABLE IF EXISTS `s_offer_click_log_stat_ad`;

CREATE TABLE `s_offer_click_log_stat_ad` (
  `id` BIGINT(22) NOT NULL AUTO_INCREMENT,
  `ad_id` CHAR(32) NOT NULL,
  `click_date` DATE NOT NULL,
  `click_total` INT(11) NOT NULL COMMENT '点击数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `click_date` (`click_date`,`ad_id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='广告点击统计';

/*Table structure for table `s_push_click_stat` */

DROP TABLE IF EXISTS `s_push_click_stat`;

CREATE TABLE `s_push_click_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `click_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app` (`click_date`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='点击统计';

/*Table structure for table `s_push_down_stat` */

DROP TABLE IF EXISTS `s_push_down_stat`;

CREATE TABLE `s_push_down_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `down_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app` (`down_date`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='下载统计';

/*Table structure for table `s_push_stat_ad` */

DROP TABLE IF EXISTS `s_push_stat_ad`;

CREATE TABLE `s_push_stat_ad` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `push_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  `push_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_ad` (`push_date`,`ad_id`,`push_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `s_push_stat_app` */

DROP TABLE IF EXISTS `s_push_stat_app`;

CREATE TABLE `s_push_stat_app` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `push_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  `push_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`push_date`,`app_id`,`push_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `s_push_stat_app_h` */

DROP TABLE IF EXISTS `s_push_stat_app_h`;

CREATE TABLE `s_push_stat_app_h` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `push_date` date NOT NULL DEFAULT '0000-00-00',
  `push_h` int(11) NOT NULL DEFAULT '0',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  `push_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`push_date`,`app_id`,`push_h`,`push_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='保留30天';

/*Table structure for table `s_push_transfer_stat` */

DROP TABLE IF EXISTS `s_push_transfer_stat`;

CREATE TABLE `s_push_transfer_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `transfer_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '激活次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app` (`transfer_date`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='广告激活统计';

/*Table structure for table `s_push_transfer_stat_ad` */

DROP TABLE IF EXISTS `s_push_transfer_stat_ad`;

CREATE TABLE `s_push_transfer_stat_ad` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `transfer_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '激活次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_ad` (`transfer_date`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按ad推送广告激活统计';

/*Table structure for table `s_push_transfer_stat_h` */

DROP TABLE IF EXISTS `s_push_transfer_stat_h`;

CREATE TABLE `s_push_transfer_stat_h` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `transfer_date` date NOT NULL DEFAULT '0000-00-00',
  `transfer_h` int(11) NOT NULL DEFAULT '0',
  `nums_pv` int(11) NOT NULL COMMENT '激活次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app_h` (`transfer_date`,`app_id`,`transfer_h`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='广告激活统计';

/*Table structure for table `s_push_view_stat` */

DROP TABLE IF EXISTS `s_push_view_stat`;

CREATE TABLE `s_push_view_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL DEFAULT '',
  `view_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  `push_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app` (`view_date`,`app_id`,`push_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='view统计';

DROP TABLE IF EXISTS `s_push_view_stat_ad`;

CREATE TABLE `s_push_view_stat_ad` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `view_date` date NOT NULL DEFAULT '0000-00-00',
  `nums_pv` int(11) NOT NULL COMMENT '点击次数',
  `push_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_ad` (`view_date`,`ad_id`,`push_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='view统计';
/*Table structure for table `s_server_stat` */

DROP TABLE IF EXISTS `s_server_stat`;

CREATE TABLE `s_server_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serv_id` int(11) NOT NULL,
  `click_date` date NOT NULL,
  `pv` int(11) NOT NULL,
  `ad` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servid` (`serv_id`,`click_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `s_transfer_stat_ad` */

DROP TABLE IF EXISTS `s_transfer_stat_ad`;

CREATE TABLE `s_transfer_stat_ad` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL,
  `transfer_date` date NOT NULL COMMENT '日期',
  `rmb_total` int(11) DEFAULT '0' COMMENT '金额',
  `transfer_total` int(11) DEFAULT '0' COMMENT '推广数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_ad` (`transfer_date`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

/*Table structure for table `s_transfer_stat_app` */

DROP TABLE IF EXISTS `s_transfer_stat_app`;

CREATE TABLE `s_transfer_stat_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transfer_date` date NOT NULL COMMENT '日期',
  `user_id` char(32) NOT NULL,
  `app_id` char(32) NOT NULL COMMENT '应用id',
  `rmb_total` int(11) DEFAULT '0' COMMENT '金额',
  `transfer_total` int(11) DEFAULT '0' COMMENT '推广数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transfer_date` (`transfer_date`,`app_id`),
  KEY `user_id` (`transfer_date`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `s_transfer_stat_app_ad` */

DROP TABLE IF EXISTS `s_transfer_stat_app_ad`;

CREATE TABLE `s_transfer_stat_app_ad` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transfer_date` date NOT NULL COMMENT '日期',
  `ad_id` char(32) NOT NULL,
  `app_id` char(32) NOT NULL COMMENT '应用id',
  `rmb_total` int(11) DEFAULT '0' COMMENT '金额',
  `transfer_total` int(11) DEFAULT '0' COMMENT '推广数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_app_ad` (`transfer_date`,`app_id`,`ad_id`),
  UNIQUE KEY `date_ad_app` (`transfer_date`,`ad_id`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='两月删除，一月查询';

/*Table structure for table `s_transfer_stat_app_h` */

DROP TABLE IF EXISTS `s_transfer_stat_app_h`;

CREATE TABLE `s_transfer_stat_app_h` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transfer_date` date NOT NULL COMMENT '日期',
  `transfer_h` int(11) NOT NULL,
  `app_id` char(32) NOT NULL COMMENT '应用id',
  `rmb_total` int(11) DEFAULT '0' COMMENT '金额',
  `transfer_total` int(11) DEFAULT '0' COMMENT '推广数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `transfer_date` (`transfer_date`,`app_id`,`transfer_h`),
  KEY `user_id` (`transfer_date`,`transfer_h`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `s_transfer_stat_channel` */

DROP TABLE IF EXISTS `s_transfer_stat_channel`;

CREATE TABLE `s_transfer_stat_channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `transfer_date` date NOT NULL COMMENT '日期',
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `rmb_total` int(11) DEFAULT '0' COMMENT '金额',
  `transfer_total` int(11) DEFAULT '0' COMMENT '推广数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`transfer_date`,`channel`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_black` */

DROP TABLE IF EXISTS `t_ad_black`;

CREATE TABLE `t_ad_black` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pack_name` varchar(50) NOT NULL,
  `app_id` char(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pack_name` (`pack_name`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_job` */

DROP TABLE IF EXISTS `t_ad_job`;

CREATE TABLE `t_ad_job` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL,
  `jobtype` int(11) NOT NULL COMMENT '0下线1上线2加分',
  `at_every` varchar(6) NOT NULL,
  `jobtime` datetime NOT NULL,
  `jobrmb` int(11) NOT NULL DEFAULT '0',
  `create_user` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `is_run` int(11) NOT NULL DEFAULT '-1' COMMENT '-2管理员删除-1创建失败0运行中1已经运行',
  `linkid` int(11) NOT NULL DEFAULT '-1' COMMENT 't_job表id',
  PRIMARY KEY (`id`),
  KEY `adid` (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_job_log` */

DROP TABLE IF EXISTS `t_ad_job_log`;

CREATE TABLE `t_ad_job_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ajid` int(11) NOT NULL,
  `ctime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_labels` */

DROP TABLE IF EXISTS `t_ad_labels`;

CREATE TABLE `t_ad_labels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(20) DEFAULT NULL COMMENT '标签内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_rules` */

DROP TABLE IF EXISTS `t_ad_rules`;

CREATE TABLE `t_ad_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL,
  `package` varchar(100) NOT NULL,
  `adjustment` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adid` (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_ad_source` */

DROP TABLE IF EXISTS `t_ad_source`;

CREATE TABLE `t_ad_source` (
  `id` varchar(32) NOT NULL COMMENT '对应t_adinfo中的id',
  `url` varchar(500) NOT NULL DEFAULT '' COMMENT '广告后台地址',
  `user` varchar(45) DEFAULT NULL COMMENT '账号',
  `pwd` varchar(45) DEFAULT NULL COMMENT '密码',
  `owner` varchar(20) DEFAULT NULL COMMENT '点乐商务人员姓名',
  `channel` varchar(20) DEFAULT NULL COMMENT '渠道名',
  `cid` varchar(50) DEFAULT NULL COMMENT '渠道号',
  `feedback` tinyint(3) unsigned DEFAULT '0' COMMENT '数据反馈方式',
  `cycle` tinyint(3) unsigned DEFAULT '0' COMMENT '数据反馈的时间周期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `t_adinfo` */

DROP TABLE IF EXISTS `t_adinfo`;

CREATE TABLE `t_adinfo` (
  `id` char(32) NOT NULL DEFAULT '',
  `in_name` varchar(100) NOT NULL default '' COMMENT '管理界面名',
  `ad_name` varchar(100) DEFAULT NULL,
  `ad_text` varchar(200) DEFAULT NULL,
  `pack_name` varchar(100) NOT NULL,
  `ad_url` varchar(200) DEFAULT NULL,
  `ad_app_type` int(11) DEFAULT NULL,
  `pic_path` varchar(100) DEFAULT NULL,
  `ad_desc` varchar(500) DEFAULT NULL,
  `create_user` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `cpc_cpa` char(3) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `ad_shoot` varchar(200) DEFAULT NULL,
  `seq_rmb` int(11) DEFAULT '0',
  `step_rmb` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '点击一次扣分',
  `quote_rmb` int(11) DEFAULT NULL,
  `others` varchar(50) DEFAULT NULL,
  `cate` int(11) NOT NULL DEFAULT '0',
  `active_time` int(11) NOT NULL DEFAULT '0' COMMENT '打开时间后通知加分，单位秒',
  `status_time` datetime NOT NULL,
  `before_tips` varchar(200) NOT NULL default '',
  `setup_tips` varchar(200) NOT NULL default '',
  `signin_tips` varchar(200) NOT NULL default '' COMMENT '签到提示语',
  `ratio` decimal(5,4) NOT NULL DEFAULT '1.0000',
  `ad_lib` varchar(20) NOT NULL COMMENT '版本',
  `ad_size` varchar(10) NOT NULL COMMENT '大小',
  `put_level` tinyint(4) NOT NULL DEFAULT '3' COMMENT '投放级别',
  `imsi` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0全投放1只投放imsi',
  `put_net` tinyint(4) NOT NULL DEFAULT '0' COMMENT '投放网络0全投1wifi23g',
  `ad_sdk_type` tinyint(1) unsigned zerofill NOT NULL DEFAULT '1',
  `ad_type` int(10) unsigned DEFAULT NULL COMMENT '广告类型',
  `net_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:all;1:china mobile;2:china unicom;3:china telecom',
  `banner_url` varchar(100) NOT NULL default '',
  `rating` tinyint(2) unsigned DEFAULT '0',
  `auto_activate` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ad_name` (`ad_name`),
  KEY `pack_name` (`pack_name`),
  KEY `seq_rmb` (`seq_rmb`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_adinfo_ios` */

DROP TABLE IF EXISTS `t_adinfo_ios`;

CREATE TABLE `t_adinfo_ios` (
  `ad_id` char(32) NOT NULL,
  `put_jb` tinyint(3) unsigned DEFAULT '0' COMMENT '是否只投放越狱市场',
  `put_ipad` tinyint(3) unsigned DEFAULT '0' COMMENT '是否只投放ipad',
  `salt` varchar(16) DEFAULT NULL COMMENT '加分校验码',
  `click_url` varchar(200) DEFAULT NULL COMMENT '通知地址',
  `ip` varchar(200) DEFAULT NULL COMMENT '客户服务器ip',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_adinfo_rmb` */

DROP TABLE IF EXISTS `t_adinfo_rmb`;

CREATE TABLE `t_adinfo_rmb` (
  `id` char(32) NOT NULL DEFAULT '',
  `rmb` int(11) DEFAULT '0',
  `rmb_in` bigint(11) DEFAULT '0',
  `rmb_out` bigint(11) DEFAULT '0',
  `money_time` datetime DEFAULT NULL COMMENT '最后一次rmb操作的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_admin` */

DROP TABLE IF EXISTS `t_admin`;

CREATE TABLE `t_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `QQ` varchar(16) NOT NULL,
  `permission` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1开启0关闭',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_adquote` */

DROP TABLE IF EXISTS `t_adquote`;

CREATE TABLE `t_adquote` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ad_id` char(32) NOT NULL COMMENT '广告id',
  `quote_date` date NOT NULL COMMENT '报价日期',
  `quote_rmb` int(11) unsigned NOT NULL COMMENT '单价',
  `nums` int(11) unsigned NOT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `adid` (`ad_id`,`quote_date`),
  KEY `quote_date` (`quote_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_app_black` */

DROP TABLE IF EXISTS `t_app_black`;

CREATE TABLE `t_app_black` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL COMMENT '应用id',
  `pack_name` varchar(50) NOT NULL COMMENT 'pack name of the ad',
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='应用不显示的广告';

/*Table structure for table `t_app_callback` */

DROP TABLE IF EXISTS `t_app_callback`;

CREATE TABLE `t_app_callback` (
  `app_id` char(32) NOT NULL,
  `api_url` varchar(200) NOT NULL,
  `api_key` varchar(32) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_appinfo` */

DROP TABLE IF EXISTS `t_appinfo`;

CREATE TABLE `t_appinfo` (
  `id` CHAR(32) NOT NULL DEFAULT '',
  `user_id` CHAR(32) DEFAULT '',
  `app_type` INT(11) DEFAULT 0,
  `appname` VARCHAR(50) DEFAULT NULL,
  `status` INT(11) DEFAULT NULL COMMENT '-1删除0新建1发布',
  `rmb_in` BIGINT(13) DEFAULT NULL,
  `create_time` DATETIME DEFAULT NULL,
  `app_icon` CHAR(60) DEFAULT NULL,
  `app_path` CHAR(200) DEFAULT NULL,
  `app_version` VARCHAR(10) DEFAULT NULL,
  `up_info` VARCHAR(200) NOT NULL DEFAULT '',
  `app_money` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '自定义货币',
  `app_ratio` FLOAT NOT NULL DEFAULT '1' COMMENT '兑换比例',
  `app_tags` VARCHAR(200) DEFAULT NULL,
  `package_name` VARCHAR(100) DEFAULT NULL,
  `list_bg` VARCHAR(60) DEFAULT NULL,
  `rmb_switch` INT(11) NOT NULL DEFAULT '1',
  `upload_app` VARCHAR(70) DEFAULT NULL COMMENT '审核用，上传应用保存路径',
  `app_desc` VARCHAR(200) DEFAULT NULL COMMENT '审核用，应用描述',
  `app_pic` VARCHAR(200) DEFAULT NULL COMMENT '审核用，应用截图保存路径',
  `open_give` INT(11) NOT NULL DEFAULT '0' COMMENT '开启送分接口',
  `update_time` DATETIME DEFAULT NULL COMMENT '修改时间',
  `level` TINYINT(4) NOT NULL DEFAULT '3' COMMENT '接收级别',
  `tags` INT(11) NOT NULL DEFAULT '0' COMMENT '接收标签',
  `push_status` TINYINT(4) NOT NULL DEFAULT '0',
  `push_interval` INT(11) NOT NULL DEFAULT '28800',
  `push_delay` INT(11) NOT NULL DEFAULT '28800',
  `push_infovisible` TINYINT(4) NOT NULL DEFAULT '0',
  `daily_limit` TINYINT(4) NOT NULL DEFAULT '1',
  `clearable` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`),
  KEY `appname` (`appname`),
  KEY `ctime` (`create_time`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Table structure for table `t_apply` */

DROP TABLE IF EXISTS `t_apply`;

CREATE TABLE `t_apply` (
  `id` char(32) NOT NULL,
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `apply_user` varchar(50) DEFAULT NULL COMMENT '申请人',
  `rmb` int(11) DEFAULT NULL COMMENT '申请额度',
  `approve_user` varchar(30) DEFAULT NULL COMMENT '审批人',
  `in_out` char(3) DEFAULT NULL COMMENT 'in广告主充值out开发者提现',
  `status` int(11) DEFAULT NULL COMMENT '-2数据库错误-1申请失败0申请中1申请成功',
  `remark` varchar(200) DEFAULT NULL,
  `is_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_apply_log` */

DROP TABLE IF EXISTS `t_apply_log`;

CREATE TABLE `t_apply_log` (
  `approval_user` varchar(30) DEFAULT NULL COMMENT '操作者',
  `apply_id` char(32) DEFAULT NULL COMMENT '申请id',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `operation` tinyint(4) DEFAULT NULL COMMENT '操作'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `t_appparam` */

DROP TABLE IF EXISTS `t_appparam`;

CREATE TABLE `t_appparam` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `p_key` varchar(32) NOT NULL DEFAULT '',
  `p_value` varchar(128) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_check_waps` */

DROP TABLE IF EXISTS `t_check_waps`;

CREATE TABLE `t_check_waps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_time` datetime NOT NULL,
  `adname` varchar(50) NOT NULL,
  `money` varchar(50) NOT NULL,
  `cpa_type` varchar(20) NOT NULL,
  `seq` int(11) NOT NULL,
  `url` varchar(700) DEFAULT NULL,
  `down_url` varchar(700) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `check_time` (`check_time`),
  KEY `adname` (`adname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_invalid_device` */

DROP TABLE IF EXISTS `t_invalid_device`;

CREATE TABLE `t_invalid_device` (
  `device_id` varchar(50) NOT NULL,
  `id_key` int(11) NOT NULL DEFAULT '0',
  `setup_count` int(11) NOT NULL COMMENT 'some devices have too many setups',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_ios_offer_click_history` */

DROP TABLE IF EXISTS `t_ios_offer_click_history`;

CREATE TABLE `t_ios_offer_click_history` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `click_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='按月建表并删除';

/*Table structure for table `t_ios_offer_click_log` */

DROP TABLE IF EXISTS `t_ios_offer_click_log`;

CREATE TABLE `t_ios_offer_click_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `snuid` varchar(32) NOT NULL,
  `click_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`),
  KEY `device_ad` (`device_id`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天删除';

/*Table structure for table `t_job` */

DROP TABLE IF EXISTS `t_job`;

CREATE TABLE `t_job` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `stamp` bigint(22) NOT NULL,
  `next` int(11) NOT NULL COMMENT '下一次执行时间间隔',
  `url` varchar(64) NOT NULL,
  `is_run` int(11) NOT NULL DEFAULT '0',
  `msg` varchar(200) DEFAULT NULL COMMENT '错误信息',
  PRIMARY KEY (`id`),
  KEY `stamp` (`stamp`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_job_log` */

DROP TABLE IF EXISTS `t_job_log`;

CREATE TABLE `t_job_log` (
  `id` char(32) NOT NULL,
  `job_time` datetime DEFAULT NULL,
  `ad_id` varchar(50) DEFAULT NULL,
  `rmb` int(11) DEFAULT NULL COMMENT '申请额度',
  `approve_user` varchar(30) DEFAULT NULL COMMENT '审批人',
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_offer_adlist_log` */

DROP TABLE IF EXISTS `t_offer_adlist_log`;

CREATE TABLE `t_offer_adlist_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `net` varchar(20) NOT NULL COMMENT '网络类型',
  `click_time` datetime NOT NULL COMMENT '点击时间',
  `packagename` varchar(200) NOT NULL,
  `sdkversion` varchar(32) NOT NULL default '',
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='广告墙打开记录,按天删除';

/*Table structure for table `t_offer_callback` */

DROP TABLE IF EXISTS `t_offer_callback`;

CREATE TABLE `t_offer_callback` (
  `id` char(32) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `snuid` varchar(50) NOT NULL,
  `rmb` int(11) NOT NULL,
  `rmb_detail` varchar(100) NOT NULL,
  `trade_time` datetime NOT NULL,
  `trade_type` int(11) NOT NULL,
  `ad_id` char(32) NOT NULL,
  `app_id` char(32) NOT NULL,
  `is_callback` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_offer_click_log` */

DROP TABLE IF EXISTS `t_offer_click_log`;

CREATE TABLE `t_offer_click_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `click_time` datetime DEFAULT NULL,
  `packagename` varchar(200) NOT NULL,
  `sdkversion` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`),
  KEY `time_ad` (`click_time`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天删除';

/*Table structure for table `t_offer_downok_log` */

DROP TABLE IF EXISTS `t_offer_downok_log`;

CREATE TABLE `t_offer_downok_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` char(32) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `ad_id` char(32) NOT NULL,
  `ctime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ctime_app` (`ctime`,`app_id`),
  KEY `ctime_ad` (`ctime`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='下载成功记录,按天删除';

/*Table structure for table `t_offer_mid_click_log` */

DROP TABLE IF EXISTS `t_offer_mid_click_log`;

CREATE TABLE `t_offer_mid_click_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `channel` varchar(32) NOT NULL,
  `click_time` datetime DEFAULT NULL,
  `packagename` varchar(200) NOT NULL,
  `sdkversion` varchar(32) NOT NULL default '',
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天删除';

/*Table structure for table `t_offer_today` */

DROP TABLE IF EXISTS `t_offer_today`;

CREATE TABLE `t_offer_today` (
  `device_id` VARCHAR(50) NOT NULL COMMENT '设备',
  `transfer_count` INT(11) NOT NULL DEFAULT '0',
  `last_transfer` DATE NOT NULL,
  PRIMARY KEY (`device_id`,`last_transfer`)
) ENGINE=INNODB DEFAULT CHARSET=utf8
COMMENT='每天早上0点到次日 0点为一个周期，统计offer激活次数';

/*Table structure for table `t_offer_trade_history_201303` */

DROP TABLE IF EXISTS `t_offer_trade_history_201303`;

CREATE TABLE `t_offer_trade_history_201303` (
  `trade_time` datetime DEFAULT NULL,
  `trade_type` int(11) DEFAULT NULL COMMENT '1广告积分到用户虚拟2用户虚拟到开发者虚拟3开发者赠送虚拟值到用户',
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `pts` int(11) DEFAULT NULL COMMENT '虚拟值',
  `rmb_detail` varchar(100) DEFAULT NULL COMMENT '汇率',
  `is_callback` int(11) NOT NULL DEFAULT '0' COMMENT '0不通知，1未通知，2通知成功，3不再通知，4超时不再通知',
  `snuid` varchar(50) NOT NULL COMMENT '应用规定的用户id',
  KEY `app_id` (`app_id`),
  KEY `device_id` (`device_id`),
  KEY `trade_time` (`trade_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_offer_trade_log` */

DROP TABLE IF EXISTS `t_offer_trade_log`;

CREATE TABLE `t_offer_trade_log` (
  `id` char(32) NOT NULL,
  `trade_time` datetime DEFAULT NULL,
  `trade_type` int(11) DEFAULT NULL COMMENT '1广告积分到用户虚拟2用户虚拟到开发者虚拟3开发者赠送虚拟值到用户',
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `rmb` int(11) DEFAULT '0',
  `rmb_detail` varchar(100) DEFAULT NULL COMMENT '汇率',
  `is_callback` int(11) NOT NULL DEFAULT '0' COMMENT '0不通知，1未通知，2通知成功，3不再通知，4超时不再通知',
  `snuid` varchar(50) NOT NULL DEFAULT '' COMMENT '应用规定的用户id',
  `packagename` varchar(200) DEFAULT NULL,
  `sdkversion` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  KEY `device_id` (`device_id`),
  KEY `trade_time` (`trade_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_offer_transfer_act` */

DROP TABLE IF EXISTS `t_offer_transfer_act`;

CREATE TABLE `t_offer_transfer_act` (
  `last_date` date NOT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  KEY `device_ad` (`device_id`,`ad_id`),
  KEY `last_date` (`last_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='45天清除';

/*Table structure for table `t_offer_transfer_limit` */

DROP TABLE IF EXISTS `t_offer_transfer_limit`;

CREATE TABLE `t_offer_transfer_limit` (
  `app_id` varchar(50) NOT NULL,
  `transfer_limit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_offer_transfer_log` */

DROP TABLE IF EXISTS `t_offer_transfer_log`;

CREATE TABLE `t_offer_transfer_log` (
  `transfer_time` datetime NOT NULL COMMENT '转移时间',
  `device_id` varchar(50) NOT NULL COMMENT '设备',
  `app_id` char(32) NOT NULL COMMENT '应用',
  `ad_id` char(32) NOT NULL COMMENT '广告',
  `channel` varchar(32) NOT NULL DEFAULT 'Dianjoy_cid',
  `rmb` int(11) NOT NULL COMMENT '人民币，以分为单位',
  `stat_id` bigint(22) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`stat_id`),
  UNIQUE KEY `u_device_ad` (`device_id`,`ad_id`),
  KEY `time_app` (`transfer_time`,`app_id`),
  KEY `time_ad` (`transfer_time`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='保留两天';

/*Table structure for table `t_offer_transfer_signin` */

DROP TABLE IF EXISTS `t_offer_transfer_signin`;

CREATE TABLE `t_offer_transfer_signin` (
  `last_date` date NOT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  KEY `device_ad` (`device_id`,`ad_id`),
  KEY `last_date` (`last_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='45天清除';

/*Table structure for table `t_offer_transfer_signin_log` */

DROP TABLE IF EXISTS `t_offer_transfer_signin_log`;

CREATE TABLE `t_offer_transfer_signin_log` (
  `signin_time` datetime NOT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `ad_id` char(32) NOT NULL DEFAULT '',
  `app_id` char(32) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  KEY `time_app` (`signin_time`,`app_id`),
  KEY `time_ad` (`signin_time`,`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_push_click_log` */

DROP TABLE IF EXISTS `t_push_click_log`;

CREATE TABLE `t_push_click_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `click_time` datetime DEFAULT NULL,
  `pack_name` varchar(200) NOT NULL,
  `sdk_version` varchar(45) NOT NULL DEFAULT '2.0.0',
  PRIMARY KEY (`id`),
  KEY `time_app` (`click_time`,`app_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天清理';

/*Table structure for table `t_push_down_log` */

DROP TABLE IF EXISTS `t_push_down_log`;

CREATE TABLE `t_push_down_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `down_time` datetime DEFAULT NULL,
  `sdk_version` varchar(45) NOT NULL DEFAULT '2.0.0',
  PRIMARY KEY (`id`),
  KEY `time_app` (`down_time`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='按天清理';

/*Table structure for table `t_push_log` */

DROP TABLE IF EXISTS `t_push_log`;

CREATE TABLE `t_push_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `push_time` datetime DEFAULT NULL,
  `pack_name` varchar(200) NOT NULL,
  `push_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sdk_version` varchar(45) NOT NULL DEFAULT '2.0.0',
  PRIMARY KEY (`id`),
  KEY `time_app` (`push_time`,`app_id`),
  KEY `time_ad` (`push_time`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='两天清除';

/*Table structure for table `t_push_sort_stat` */

DROP TABLE IF EXISTS `t_push_sort_stat`;

CREATE TABLE `t_push_sort_stat` (
  `device_id` char(32) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  `num` int(11) NOT NULL DEFAULT '1',
  `last_date` date NOT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_push_today` */

DROP TABLE IF EXISTS `t_push_today`;

CREATE TABLE `t_push_today` (
  `device_id` VARCHAR(50) NOT NULL COMMENT '设备',
  `last_push` DATE NOT NULL,
  `push_count` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`,`last_push`)
) ENGINE=INNODB DEFAULT CHARSET=utf8
COMMENT='每天早上8点到次日 8点为一个周期，统计push次数和上次push的时间,8点truncate';

/*Table structure for table `t_push_transfer_log` */

DROP TABLE IF EXISTS `t_push_transfer_log`;

CREATE TABLE `t_push_transfer_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `transfer_time` datetime DEFAULT NULL,
  `pack_name` varchar(200) NOT NULL,
  `sdk_version` varchar(45) NOT NULL DEFAULT '2.0.0',
  PRIMARY KEY (`id`),
  KEY `time_app` (`transfer_time`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_push_view_log` */

DROP TABLE IF EXISTS `t_push_view_log`;

CREATE TABLE `t_push_view_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(50) DEFAULT NULL,
  `app_id` char(32) DEFAULT NULL,
  `ad_id` char(32) DEFAULT NULL,
  `view_time` datetime DEFAULT NULL,
  `pack_name` varchar(200) NOT NULL DEFAULT '',
  `push_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sdk_version` varchar(45) NOT NULL DEFAULT '2.0.0',
  PRIMARY KEY (`id`),
  KEY `time_app` (`view_time`,`app_id`),
  KEY `device_id` (`device_id`),
  KEY `time_ad` (`view_time`,`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_reward` */

DROP TABLE IF EXISTS `t_reward`;

CREATE TABLE `t_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(100) NOT NULL COMMENT '开发者帐号',
  `ratio` tinyint(4) NOT NULL DEFAULT '0' COMMENT '百分百',
  `start` date default NULL COMMENT '开始日期',
  `end` date default NULL COMMENT '结束日期',
  `reward` int(11) NOT NULL DEFAULT '0' COMMENT '奖励额度',
  `ctime` datetime NOT NULL COMMENT '创建时间',
  `is_pay` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否付款',
  `pay_id` char(32) NOT NULL default '' COMMENT '调整人，后台管理员',
  `reason` varchar(100) DEFAULT NULL COMMENT '调整开发者账户资金的缘由',
  PRIMARY KEY (`id`),
  KEY `account` (`account`(8)),
  KEY `pay_id` (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `t_server` */

DROP TABLE IF EXISTS `t_server`;

CREATE TABLE `t_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(60) NOT NULL COMMENT '服务器地址',
  `nums` int(11) NOT NULL COMMENT '权值',
  `status` int(11) NOT NULL COMMENT '状态0关闭1启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `t_server_log` */

DROP TABLE IF EXISTS `t_server_log`;

CREATE TABLE `t_server_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serv_id` int(11) NOT NULL COMMENT '服务器id',
  `ad_id` char(32) NOT NULL COMMENT '广告id',
  `link_time` datetime NOT NULL COMMENT '下载时间',
  `type` int(11) NOT NULL COMMENT '类型0没有分流1分流正常2不存在下载文件',
  PRIMARY KEY (`id`),
  KEY `serv_id` (`serv_id`),
  KEY `link_time` (`link_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='一天删除';

/*Table structure for table `t_sys_conf` */

DROP TABLE IF EXISTS `t_sys_conf`;

CREATE TABLE `t_sys_conf` (
  `sys_key` varchar(50) NOT NULL,
  `sys_value` varchar(50) NOT NULL,
  PRIMARY KEY (`sys_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_sys_const` */

DROP TABLE IF EXISTS `t_sys_const`;

CREATE TABLE `t_sys_const` (
  `sys_key` varchar(50) NOT NULL,
  `sys_value` varchar(50) NOT NULL,
  PRIMARY KEY (`sys_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_upload_log` */

DROP TABLE IF EXISTS `t_upload_log`;

CREATE TABLE `t_upload_log` (
  `id` char(32) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `upload_user` varchar(20) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` char(32) NOT NULL,
  `account` varchar(100) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `registe_time` datetime DEFAULT NULL,
  `qq` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `rmb` bigint(11) DEFAULT NULL,
  `rmb_in` bigint(11) DEFAULT NULL,
  `rmb_out` bigint(11) DEFAULT NULL,
  `admin_qq` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `identity` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_address` varchar(100) DEFAULT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `identity_pic` varchar(100) DEFAULT NULL COMMENT '身份证复印件',
  `identity_pic_back` varchar(100) NOT NULL DEFAULT '' COMMENT '',
  `ref_email` varchar(100) DEFAULT NULL COMMENT '推荐人邮箱',
  `daily` int(11) NOT NULL DEFAULT '0' COMMENT '0不发送日报，1发送日报',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`),
  KEY `time` (`registe_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_user_edit` */

DROP TABLE IF EXISTS `t_user_edit`;

CREATE TABLE `t_user_edit` (
  `eid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(100) NOT NULL,
  `qq` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `identity` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_address` varchar(100) DEFAULT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `identity_pic` varchar(100) DEFAULT NULL COMMENT '身份证复印件',
  `identity_pic_back` varchar(100) NOT NULL default '',
  `ctime` datetime NOT NULL COMMENT '提交时间',
  `is_verify` tinyint(4) NOT NULL COMMENT '0未审核，1审核通过，-1审核不通过，-2开发者放弃修改',
  `remark` VARCHAR(200) NOT NULL DEFAULT '',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  PRIMARY KEY (`eid`),
  KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=410 DEFAULT CHARSET=utf8;

/*Table structure for table `t_user_push_stop` */

DROP TABLE IF EXISTS `t_user_push_stop`;

CREATE TABLE `t_user_push_stop` (
  `device_id` varchar(50) NOT NULL,
  `last_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_user_rmb_stat` */

DROP TABLE IF EXISTS `t_user_rmb_stat`;

CREATE TABLE `t_user_rmb_stat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `userid` char(32) NOT NULL,
  `rmb_real` float(9,3) DEFAULT '0.000',
  `rmb_date` date NOT NULL DEFAULT '0000-00-00',
  `rmb_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_date_userid` (`rmb_date`,`rmb_type`,`userid`),
  KEY `i_userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=21936 DEFAULT CHARSET=utf8;

/*Table structure for table `t_user_stat` */

DROP TABLE IF EXISTS `t_user_stat`;

CREATE TABLE `t_user_stat` (
  `id` char(32) NOT NULL,
  `key` char(8) DEFAULT NULL COMMENT 'md5(用户名+dianjoy+肉山真帅）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `x_cash_log` */

DROP TABLE IF EXISTS `x_cash_log`;

CREATE TABLE `x_cash_log` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `user_id` char(32) DEFAULT NULL,
  `cash_time` datetime DEFAULT NULL,
  `rmb_in` int(11) DEFAULT NULL,
  `cash_type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time_idx` (`cash_time`),
  KEY `user_idx` (`user_id`),
  KEY `type_idx` (`cash_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `app_wandoujia`;

CREATE TABLE `app_wandoujia` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '..',
  `name` VARCHAR(45) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '...',
  `system` VARCHAR(45) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '....',
  `category` VARCHAR(45) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '...',
  `packagename` VARCHAR(200) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '..',
  `number` INT(11) NOT NULL DEFAULT '0' COMMENT '....',
  `url` VARCHAR(1000) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '..',
  `downloadurl` VARCHAR(8190) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '....',
  `checkad` VARCHAR(200) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '....',
  `checked` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '....',
  `meta` VARCHAR(45) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '....',
  `time` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ;

/*t_channel*/
DROP TABLE IF EXISTS `t_channel`;

CREATE TABLE `t_channel` (
  `id` char(32) NOT NULL,
  `account` varchar(100) DEFAULT NULL,
  `password` char(32) DEFAULT '',
  `status` int(11) DEFAULT -1,
  `channel_name` varchar(100) DEFAULT '',
  `comment` varchar(100) DEFAULT '',
  `ratio` float DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`),
  UNIQUE KEY `channel_name` (`channel_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_advertiser`;

CREATE TABLE `t_advertiser` (
  `id` char(32) NOT NULL,
  `account` varchar(100) DEFAULT NULL,
  `password` char(32) DEFAULT '',
  `status` int(11) DEFAULT -1,
  `adv_name` varchar(100) DEFAULT '',
  `comment` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`),
  UNIQUE KEY `adv_name` (`adv_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `t_newad_apply`;
CREATE TABLE `t_newad_apply` (
  `id` BIGINT(22) NOT NULL AUTO_INCREMENT,
  `ad_name` VARCHAR(100) DEFAULT NULL,
  `ad_text` VARCHAR(200) DEFAULT NULL,
  `pack_name` VARCHAR(100) NOT NULL,
  `ad_url` VARCHAR(200) DEFAULT NULL,
  `pic_path` VARCHAR(100) DEFAULT NULL,
  `status` INT(11) DEFAULT NULL,
  `ad_shoot` VARCHAR(200) DEFAULT NULL,
  `channel` VARCHAR(20) DEFAULT NULL,
  `apply_time` DATETIME NOT NULL,
  `ad_app_type` INT(11) DEFAULT '1' COMMENT 'Android1IOS2',
  `ad_type` INT(10) UNSIGNED DEFAULT '0',
  `ad_desc` VARCHAR(500) DEFAULT NULL,
  `ad_lib` VARCHAR(20) NOT NULL,
  `ad_size` VARCHAR(10) NOT NULL,
  `cpc_cpa` CHAR(3) DEFAULT 'cpa',
  `quote_rmb` INT(11) DEFAULT NULL,
  `cate` INT(11) DEFAULT '1' COMMENT 'Registe or Try',
  `quality` VARCHAR(50) DEFAULT '',
  `test_time` VARCHAR(50) DEFAULT '',
  `test_level` VARCHAR(50) DEFAULT '',
  `company_name` VARCHAR(50) DEFAULT '',
  `net_type` TINYINT(4) NOT NULL DEFAULT '0',
  `put_jb` TINYINT(3) UNSIGNED DEFAULT '0',
  `put_ipad` TINYINT(3) UNSIGNED DEFAULT '0',
  `owner` VARCHAR(20) DEFAULT NULL,
  `url` VARCHAR(500) NOT NULL DEFAULT '',
  `user` VARCHAR(45) DEFAULT NULL,
  `pwd` VARCHAR(45) DEFAULT NULL,
  `feedback` TINYINT(3) UNSIGNED DEFAULT '0' ,
  `cycle` TINYINT(3) UNSIGNED DEFAULT '0' ,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



/*for procedure `p_update_t_user_stat` */
DELIMITER $$
drop procedure IF EXISTS `p_update_t_user_stat`$$
CREATE PROCEDURE `p_update_t_user_stat`(in in_date date)
BEGIN
     declare cpm int;
     SELECT cast(sys_value as signed) into cpm FROM t_sys_conf WHERE sys_key='cpm';
     delete from t_user_rmb_stat where rmb_date = in_date;

  insert into t_user_rmb_stat (userid,rmb_real,rmb_date,rmb_type)
  SELECT i.user_id, SUM(p.nums_pv)*cpm/1000 AS rmb_real,in_date,255
FROM t_appinfo AS i
INNER JOIN
(
SELECT app_id, SUM(nums_pv) AS nums_pv
FROM `s_push_stat_app` WHERE push_date = in_date
GROUP BY app_id
) AS p ON p.app_id = i.id
GROUP BY i.user_id;

insert into t_user_rmb_stat (userid,rmb_real,rmb_date,rmb_type)
SELECT user_id,SUM(rmb) AS rmb_real,in_date,app_type
FROM t_appinfo i
JOIN
(
SELECT app_id, SUM(rmb_total)/100 AS rmb
FROM `s_transfer_stat_app` t
WHERE transfer_date = in_date
GROUP BY app_id
) AS p ON p.app_id = i.id
GROUP BY user_id,app_type;

END$$
DELIMITER ;
/*for event `update_t_user_rmb_stat` */
drop event if EXISTS `update_t_user_rmb_stat`;

CREATE EVENT `update_t_user_rmb_stat` ON SCHEDULE EVERY 1 DAY STARTS
'2013-02-20 03:45:00' ON COMPLETION PRESERVE ENABLE
DO CALL p_update_t_user_stat(DATE_ADD(CURDATE(),INTERVAL -1 DAY));

DELIMITER $$
DROP PROCEDURE IF EXISTS `delete_history`$$

CREATE
    PROCEDURE delete_history(IN tb VARCHAR(50), IN where_sql VARCHAR(100))
    BEGIN
	DECLARE cout INT;
	SET cout=10;
	WHILE cout>0 DO
		SET @sql = CONCAT('DELETE FROM ',tb, ' WHERE ',where_sql, ' LIMIT 3000');
		PREPARE STMT FROM @sql;
		EXECUTE STMT;
		SELECT ROW_COUNT() INTO cout;
	END WHILE;
    END$$

DELIMITER ;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;