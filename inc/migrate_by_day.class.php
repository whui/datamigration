<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-15
 * Time: 上午10:44
 * To change this template use File | Settings | File Templates.
 */
include_once("migrate.class.php");
class migrate_by_day extends migrate
{
    var $date_column;
    var $look_back_str;
    function __construct($name,$input,$output,$date_column,$lookbackdaydate_str)
    {
        parent::__construct($name,$input,$output);
        $this->date_column=$date_column;
        $this->look_back_str=$lookbackdaydate_str;
    }
    function process()
    {
        echo'\r\n';
        $end=date("Y-m-d");
        $sql_start_day = "SELECT DATE_ADD(MAX(".$this->date_column."),INTERVAL 1 DAY) FROM ".$this->table_name;
        $start = $this->dbh_new->query($sql_start_day)->fetchColumn();
        if(!$start)
        {
            $start = date('Y-m-d',strtotime($this->look_back_str));
        }
        for($i=strtotime($start);$i<=strtotime($end);$i+=86400)
        {
            $date = date("Y-m-d",$i);
            echo $date;
            $this->insert_one_day($date);
        }

        echo 'finish';
    }

    function insert_one_day($date)
    {
        $sql = str_replace("%s", $date,$this->input_sql);
        $result=$this->dbh_old->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $values=array();
        foreach((array)$result as $line){
            $values[]="('".implode("','",$line)."')";
        }
        if($values){
            $valuesql=implode(",",$values);
            $sql = $this->output_sql.$valuesql;
            $count=$this->dbh_new->exec($sql);
            echo $this->table_name;
            if(!$count)
            {
                echo "insert false or zero:";
                echo $sql;
            }
            echo $count;
        }
    }
}
