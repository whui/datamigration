<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-15
 * Time: 上午10:43
 * To change this template use File | Settings | File Templates.
 */

class migrate
{
    var $dbh_old = null;
    var $dbh_new = null;
    var $input_sql;
    var $output_sql;
    var $table_name;
    function __construct($name,$input,$output)
    {
        $this->dbh_old = include(dirname(__FILE__)."/pdo_old.php");

        $this->dbh_new = include(dirname(__FILE__)."/pdo_new.php");
        $this->input_sql=$input;
        $this->output_sql=$output;
        $this->table_name=$name;
    }

    function process()
    {
        echo'\r\n';
        $result=$this->dbh_old->query($this->input_sql)->fetchAll(PDO::FETCH_ASSOC);
        $values=null;
        foreach((array)$result as $line){
            $values[]="('".implode("','",$line)."')";
        }
        if($values){
            $valuesql=implode(",",$values);
            $sql = $this->output_sql.$valuesql;
            $count=$this->dbh_new->exec($sql);
            echo $this->table_name;
            if(!$count)
            {
                echo 'return false';
                echo $sql;
            }
            echo $count;
        }
        echo 'finish';
    }
}