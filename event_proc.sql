
DELIMITER $$
drop procedure IF EXISTS `p_update_t_user_stat`$$
CREATE PROCEDURE `p_update_t_user_stat`(in in_date date)
BEGIN
     declare cpm int;
     SELECT cast(sys_value as signed) into cpm FROM t_sys_conf WHERE sys_key='cpm';
     delete from t_user_rmb_stat where rmb_date = in_date;

  insert into t_user_rmb_stat (userid,rmb_real,rmb_date,rmb_type)
  SELECT i.user_id, SUM(p.nums_pv)*cpm/1000 AS rmb_real,in_date,255
FROM t_appinfo AS i
INNER JOIN
(
SELECT app_id, SUM(nums_pv) AS nums_pv
FROM `s_push_stat_app` WHERE push_date = in_date
GROUP BY app_id
) AS p ON p.app_id = i.id
GROUP BY i.user_id;

insert into t_user_rmb_stat (userid,rmb_real,rmb_date,rmb_type)
SELECT user_id,SUM(rmb) AS rmb_real,in_date,app_type 
FROM t_appinfo i
JOIN
(
SELECT app_id, SUM(rmb_total)/100 AS rmb
FROM `s_transfer_stat_app` t
WHERE transfer_date = in_date
GROUP BY app_id
) AS p ON p.app_id = i.id
GROUP BY user_id,app_type;

END$$

DELIMITER ;

drop event if EXISTS `update_t_user_rmb_stat`;

CREATE EVENT `update_t_user_rmb_stat` ON SCHEDULE EVERY 1 DAY STARTS 
'2013-02-20 03:45:00' ON COMPLETION PRESERVE ENABLE 
DO CALL p_update_t_user_stat(DATE_ADD(CURDATE(),INTERVAL -1 DAY));

DELIMITER $$
DROP PROCEDURE IF EXISTS `delete_history`$$

CREATE
    PROCEDURE delete_history(IN tb VARCHAR(50), IN where_sql VARCHAR(100))
    BEGIN
	DECLARE cout INT;
	SET cout=10;
	WHILE cout>0 DO
		SET @sql = CONCAT('DELETE FROM ',tb, ' WHERE ',where_sql, ' LIMIT 10000');
		PREPARE STMT FROM @sql;
		EXECUTE STMT;
		SELECT ROW_COUNT() INTO cout;
	END WHILE;
    END$$

DELIMITER ;