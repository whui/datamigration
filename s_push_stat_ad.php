<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-12
 * Time: 下午2:44
 * To change this template use File | Settings | File Templates.
 */

include_once("inc/migrate_by_day.class.php");
$input_sql = "SELECT
	`ad_id`,
	'%s' AS `push_date`,
	SUM(`nums_pv`) AS `nums_pv`,
	`push_type`
	FROM
	`t_push_log_stat`
	WHERE `push_date`='%s' AND push_h =24
	GROUP BY ad_id,push_type";
$outpt_sql ="INSERT INTO `s_push_stat_ad`
	(
	`ad_id`,
	`push_date`,
	`nums_pv`,
	`push_type`
	)
	values";
$mig = new migrate_by_day("s_push_stat_ad",$input_sql,$outpt_sql,"push_date","-1 year");
$mig->process();
?>