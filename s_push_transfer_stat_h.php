<?php
/**
 * Created by JetBrains PhpStorm.
 * User: HUI
 * Date: 13-4-12
 * Time: 下午2:44
 * To change this template use File | Settings | File Templates.
 */
include_once("inc/migrate_by_day.class.php");
$input_sql = "SELECT
	`app_id`,
	'%s' AS `transfer_date`,
	transfer_h,
	SUM(`nums_pv`) AS `nums_pv`
	FROM
	`t_push_transfer_log_stat`
	WHERE `transfer_date`='%s' AND transfer_h <24
	GROUP BY app_id,transfer_h";
$outpt_sql ="INSERT INTO `s_push_transfer_stat_h`
	(
	`app_id`,
	`transfer_date`,
	`transfer_h`,
	`nums_pv`
	)
	VALUES";
$mig = new migrate_by_day("s_push_transfer_stat_h",$input_sql,$outpt_sql,"transfer_date","-90 day");
$mig->process();
?>