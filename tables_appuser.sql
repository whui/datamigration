/*
SQLyog Community v11.01 (64 bit)
MySQL - 5.5.16 : Database - dianjoy
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `t_appuser` */

DROP TABLE IF EXISTS `t_appuser`;

CREATE TABLE `t_appuser` (
  `appid_key` int(11) NOT NULL,
  `app_id` char(32) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `first_date` date NOT NULL,
  `last_date` date NOT NULL,
  `ua` varchar(50) NOT NULL,
  UNIQUE KEY `appid_key` (`appid_key`,`app_id`,`device_id`),
  KEY `last_date` (`last_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY HASH (appid_key)
PARTITIONS 16 */;

/*Table structure for table `t_appuser_rmb` */

DROP TABLE IF EXISTS `t_appuser_rmb`;

CREATE TABLE `t_appuser_rmb` (
  `app_id` char(32) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `pts` int(11) NOT NULL DEFAULT '0',
  `pts_in` int(11) NOT NULL DEFAULT '0',
  `pts_out` int(11) NOT NULL DEFAULT '0',
  `last_date` date NOT NULL,
  UNIQUE KEY `appid_device` (`app_id`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
